import RatingDetails from './RatingDetails/RatingDetails';
import classes from './RatingsList.module.css';

export default function RatingsList(props) {
  return (
    <div className={classes.ratingsList}>
      <h1>employee feedback data</h1>
      <div className={classes.allRatings}>
        {props.entries.map((entry) => (
          <RatingDetails {...entry} />
        ))}
      </div>
      <button className={classes.backBtn} onClick={props.onClickingBack}>
        Go Back
      </button>
    </div>
  );
}
