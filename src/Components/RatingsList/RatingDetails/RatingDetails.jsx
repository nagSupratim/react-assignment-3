import classes from './RatingDetails.module.css';
export default function RatingDetails({ name, department, rating }) {
  return (
    <div className={classes.rating}>
      {`Name : ${name} | Department : ${department} | Rating : ${rating}`}
    </div>
  );
}
