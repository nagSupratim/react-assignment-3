import { useRef } from 'react';
import classes from './FeedbackForm.module.css';

export default function FeedbackForm(props) {
  const inputNameRef = useRef();
  const inputDepartmentRef = useRef();
  const inputRatingRef = useRef();

  const formSubmitHandler = (e) => {
    e.preventDefault();
    const newEntry = {
      id: Date.now(),
      name: inputNameRef.current.value,
      department: inputDepartmentRef.current.value,
      rating: inputRatingRef.current.value,
    };
    props.onNewEntry(newEntry);
    inputNameRef.current.value = '';
    inputDepartmentRef.current.value = '';
    inputRatingRef.current.value = '';
  };
  return (
    <div className={classes.form}>
      <h1>employee feedback from</h1>
      <form onSubmit={formSubmitHandler}>
        <div className={classes.singleInput}>
          <label htmlFor="emp_name">name :</label>
          <input type="text" name="Name" id="emp_name" ref={inputNameRef} />
        </div>
        <div className={classes.singleInput}>
          <label htmlFor="emp_department">department :</label>
          <input
            type="text"
            name="Department"
            id="emp_department"
            ref={inputDepartmentRef}
          />
        </div>
        <div className={classes.singleInput}>
          <label htmlFor="emp_name">rating :</label>
          <input
            type="number"
            name="rating"
            id="emp_rating"
            ref={inputRatingRef}
          />
        </div>
        <div className={classes.actions}>
          <input type="submit" value="Submit" />
        </div>
      </form>
    </div>
  );
}
