import { useState } from 'react';
import FeedbackForm from './Components/FeedbackForm/FeedbackForm';
import RatingsList from './Components/RatingsList/RatingsList';

function App() {
  const [allEmployeeFeedbacks, setAllEmployeeFeedbacks] = useState([]);
  const [showForms, setShowForms] = useState(true);
  const onNewEmployeeAdd = (entry) => {
    setAllEmployeeFeedbacks((prev) => [entry, ...prev]);
    setShowForms(false);
  };
  const onGoingBack = () => {
    setShowForms(true);
  };
  return (
    <>
      {showForms && <FeedbackForm onNewEntry={onNewEmployeeAdd} />}
      {!showForms && (
        <RatingsList
          entries={allEmployeeFeedbacks}
          onClickingBack={onGoingBack}
        />
      )}
    </>
  );
}

export default App;
